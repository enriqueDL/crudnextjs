import React, {useState} from 'react';


const FormTarea = () => {

	const [input, setInput] = useState('')
	return (

			<form className="todo-form">
				<div className="contenedor-iput" >
					<input
					 	type="text"
					 	className="todo-iput"
					 	placeholder="Nombre Tarea..."
					 	name='text'
						 value={input}
						 

					 />
				</div>

				<button className="todo-button">Add</button>
			</form>
	
	);
	
}

export default FormTarea;