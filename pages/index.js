import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { useRouter } from 'next/router';
import { route } from 'next/dist/next-server/server/router';



function Index() {
    const [usuario, guardarUsuario] = useState({
		email: '',
		password: ''
	});

    const router = useRouter()
	// extraer de usuario
	const {email, password} = usuario;

	const onChange = e => {
		guardarUsuario({
			...usuario,
			[e.target.name] : e.target.value
		})
	}

	// Cuando el usuario quiere iniciar sesion

	const onSubmit = e => {
		e.preventDefault();

		//Validar que no haya campos vacios

		//Pasarlo al action
	}
	return (

		<div class="card" style={{width: '50%', height: '50%'}}>
			<div className="contenedor-form sombra-dark">
				<h1>Iniciar Sesión</h1>

				<form
					onSubmit={onSubmit}
				
				 >
					<div className="campo-form">
						<label htmlFor="email">Email</label>
						<input 
							type="email"
							id="email"
							name="email"
							placeholder="Tu Email"
							value={email}
							onChange={onChange} />
					</div>

					<div className="campo-form">
						<label htmlFor="password">Password</label>
						<input 
							type="password"
							id="password"
							name="password"
							placeholder="Tu Password"
							value={password}
							onChange={onChange} />
					</div>

					<div className="campo-from">
						<input type="submit" class="btn- btn-primario btn-block"
						  value="Iniciar Sesión" onClick={() => router.push('/FormTarea')} />
					</div>
				</form>
			</div>
        
		</div>

        
	);
}

export default Index;
