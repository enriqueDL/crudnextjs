/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/FormTarea";
exports.ids = ["pages/FormTarea"];
exports.modules = {

/***/ "./pages/FormTarea.js":
/*!****************************!*\
  !*** ./pages/FormTarea.js ***!
  \****************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n\nvar _jsxFileName = \"/home/sinco/proyectReact/crudnextjs/pages/FormTarea.js\";\n\n\nconst FormTarea = () => {\n  const {\n    0: input,\n    1: setInput\n  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)('');\n  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"form\", {\n    className: \"todo-form\",\n    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n      className: \"contenedor-iput\",\n      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"input\", {\n        type: \"text\",\n        className: \"todo-iput\",\n        placeholder: \"Nombre Tarea...\",\n        name: \"text\",\n        value: input\n      }, void 0, false, {\n        fileName: _jsxFileName,\n        lineNumber: 11,\n        columnNumber: 6\n      }, undefined)\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 10,\n      columnNumber: 5\n    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n      className: \"todo-button\",\n      children: \"Add\"\n    }, void 0, false, {\n      fileName: _jsxFileName,\n      lineNumber: 22,\n      columnNumber: 5\n    }, undefined)]\n  }, void 0, true, {\n    fileName: _jsxFileName,\n    lineNumber: 9,\n    columnNumber: 4\n  }, undefined);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (FormTarea);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9jcnVkbmV4dGpzLy4vcGFnZXMvRm9ybVRhcmVhLmpzP2JkYTkiXSwibmFtZXMiOlsiRm9ybVRhcmVhIiwiaW5wdXQiLCJzZXRJbnB1dCIsInVzZVN0YXRlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBR0EsTUFBTUEsU0FBUyxHQUFHLE1BQU07QUFFdkIsUUFBTTtBQUFBLE9BQUNDLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CQywrQ0FBUSxDQUFDLEVBQUQsQ0FBbEM7QUFDQSxzQkFFRTtBQUFNLGFBQVMsRUFBQyxXQUFoQjtBQUFBLDRCQUNDO0FBQUssZUFBUyxFQUFDLGlCQUFmO0FBQUEsNkJBQ0M7QUFDRSxZQUFJLEVBQUMsTUFEUDtBQUVFLGlCQUFTLEVBQUMsV0FGWjtBQUdFLG1CQUFXLEVBQUMsaUJBSGQ7QUFJRSxZQUFJLEVBQUMsTUFKUDtBQUtFLGFBQUssRUFBRUY7QUFMVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERCxlQWFDO0FBQVEsZUFBUyxFQUFDLGFBQWxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZGO0FBb0JBLENBdkJEOztBQXlCQSwrREFBZUQsU0FBZiIsImZpbGUiOiIuL3BhZ2VzL0Zvcm1UYXJlYS5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwge3VzZVN0YXRlfSBmcm9tICdyZWFjdCc7XG5cblxuY29uc3QgRm9ybVRhcmVhID0gKCkgPT4ge1xuXG5cdGNvbnN0IFtpbnB1dCwgc2V0SW5wdXRdID0gdXNlU3RhdGUoJycpXG5cdHJldHVybiAoXG5cblx0XHRcdDxmb3JtIGNsYXNzTmFtZT1cInRvZG8tZm9ybVwiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbnRlbmVkb3ItaXB1dFwiID5cblx0XHRcdFx0XHQ8aW5wdXRcblx0XHRcdFx0XHQgXHR0eXBlPVwidGV4dFwiXG5cdFx0XHRcdFx0IFx0Y2xhc3NOYW1lPVwidG9kby1pcHV0XCJcblx0XHRcdFx0XHQgXHRwbGFjZWhvbGRlcj1cIk5vbWJyZSBUYXJlYS4uLlwiXG5cdFx0XHRcdFx0IFx0bmFtZT0ndGV4dCdcblx0XHRcdFx0XHRcdCB2YWx1ZT17aW5wdXR9XG5cdFx0XHRcdFx0XHQgXG5cblx0XHRcdFx0XHQgLz5cblx0XHRcdFx0PC9kaXY+XG5cblx0XHRcdFx0PGJ1dHRvbiBjbGFzc05hbWU9XCJ0b2RvLWJ1dHRvblwiPkFkZDwvYnV0dG9uPlxuXHRcdFx0PC9mb3JtPlxuXHRcblx0KTtcblx0XG59XG5cbmV4cG9ydCBkZWZhdWx0IEZvcm1UYXJlYTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/FormTarea.js\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/FormTarea.js"));
module.exports = __webpack_exports__;

})();